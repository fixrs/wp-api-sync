<?php
declare(strict_types=1);

use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Middleware\Authentication\SimpleTokenAuthentication;
use Automattic\WooCommerce\Client as WooCommerceClient;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);

            $loggerSettings = $settings->get('logger');
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },

        SimpleTokenAuthentication::class => function (ContainerInterface $c) {
            
            $settings = $c->get(SettingsInterface::class);
 
            $options = $settings->get('auth');

            return new SimpleTokenAuthentication($c, $options);
        },

        PDO::class => function (ContainerInterface $c) {
 
            $settings = $c->get(SettingsInterface::class);
 
            $dbSettings = $settings->get('db');
 
            $host = $dbSettings['host'];
            $dbname = $dbSettings['database'];
            $username = $dbSettings['username'];
            $password = $dbSettings['password'];
            $charset = $dbSettings['charset'];
            $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";
            return new PDO($dsn, $username, $password);
        },

        WooCommerceClient::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);
 
            $dbSettings = $settings->get('woocommerce_api');
            $url = $dbSettings['url'];
            $key = $dbSettings['key'];
            $secret = $dbSettings['secret'];
            
            $woocommerce = new WooCommerceClient(
                $url, $key, $secret, [ 'version' => 'wc/v3', 'timeout' => '3000' ]
            );

            return $woocommerce;
        },
    ]);
};
