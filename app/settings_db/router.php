<?php
declare(strict_types=1);

switch ($_SERVER['HTTP_HOST']) {
    case 'dev.cardiochoc.ca' : 
        return require_once('../app/settings_db/dev.php');
        break;
    case 'www.cardiochoc.ca' :
    case 'cardiochoc.ca' :
        return require_once('../app/settings_db/prod.php');
        break;
    default : 
        return require_once('../app/settings_db/localhost.php');
        break;
}