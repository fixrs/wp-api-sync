<?php
declare(strict_types=1);

return [
    'driver' => 'mysql',
    'host' => 'localhost',
    'port' => '3306',
    'username' => 'cardioc_wp',
    'database' => 'cardioc_wp',
    'password' => 'Tld*1m42',
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'flags' => [
        // Turn off persistent connections
        PDO::ATTR_PERSISTENT => false,
        // Enable exceptions
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        // Emulate prepared statements
        PDO::ATTR_EMULATE_PREPARES => true,
        // Set default fetch mode to array
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ],
];