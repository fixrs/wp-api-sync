<?php
declare(strict_types=1);

use App\Application\Settings\Settings;
use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {

    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () {
            return new Settings([
                'displayErrorDetails' => true, // Should be set to false in production
                'logError'            => false,
                'logErrorDetails'     => false,
                'logger' => [
                    'name' => 'slim-app',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
                "auth" => [
                    // boolean - whether to enforce an https connection
                    'secure'    => true,
                    // array - list of hostnames/IP addresses to ignore the secure flag
                    'relaxed'   => ['cc_wp_sync.localhost', 'localhost', 'cc_wp_sync_api_php', '127.0.0.1'],
                    // string - the header to check for the token (set to false, null, or '' to skip)
                    'header'    => ( $_SERVER['HTTP_HOST'] == 'cc_wp_sync.localhost' || $_SERVER['HTTP_HOST'] == 'cc_wp_sync_api_php' ? 'Host' : 'X-Auth'),
                    // callable - function to validate the token [required]
                    'validate'  => function ($p_token): bool {
                        return 
                            $_SERVER['HTTP_HOST'] == 'cc_wp_sync.localhost' || $_SERVER['HTTP_HOST'] == 'cc_wp_sync_api_php' ||
                            $p_token == 'kx9PdQTHsbDNjfASwj2KY5JhnMVWDucsA9ZqQjwkqrFkAaSucvpypZR44msVmtZ52f5aY2YZ8bxcYxvNRPkq2ckZm6ccSuk2v4UARkZnrBXfQv9r4EuqkGqcJMuqF6Kz';
                    }
                ],
                "db" => require_once('../app/settings_db/router.php'),
                'woocommerce_api'   => [
                    'url' => 'https://dev.cardiochoc.ca',
                    'key' => 'ck_f840d7ec6c5af7e6a2443eba4dcf6f06f4db5d4a',
                    'secret' => 'cs_dca829d04529746c581139cb9f89cff2945209ed'
                ],
            ]);
        }
    ]);
};
