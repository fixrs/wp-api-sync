<?php
declare(strict_types=1);

use App\Application\Actions\Product\SyncProductsFromAcombaAction;
use App\Application\Actions\Product\ViewProductAction;
use App\Application\Actions\Product\ViewProductWithAcombaSkuAction;
use App\Application\Actions\Product\ViewAllSKUsWithIdsAction;
use App\Application\Actions\Client\ViewClientAction;
use App\Application\Actions\Client\ViewClientActionWithPhone;
use App\Application\Actions\Group\ListAllGroupsAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Middleware\Authentication\SimpleTokenAuthentication;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use Automattic\WooCommerce\Client as WooCommerceClient;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->group('/products', function (Group $group) {
        $group->get('/all_skus_with_ids', ViewAllSKUsWithIdsAction::class);
        $group->get('/{id:[0-9]+}', ViewProductAction::class);
        $group->get('/sku/{sku:.+}', ViewProductWithAcombaSkuAction::class);
        $group->patch('', SyncProductsFromAcombaAction::class);
    })->add(SimpleTokenAuthentication::class);

    $app->get('/groups', ListAllGroupsAction::class);

    $app->group('/clients', function (Group $group) {
        $group->get('/{id:[0-9]+}', ViewClientAction::class);
        $group->get('/phone/{phone:[0-9\-]+}', ViewClientActionWithPhone::class);
    })->add(SimpleTokenAuthentication::class);

    $app->get('/bench', function (Request $request, Response $response) {
        $wc = $this->get(WooCommerceClient::class);

        $page = isset($_GET['page']) ? $_GET['page'] : 0;
        $chunkSize = 20;

        $fullBatch = json_decode(file_get_contents('./sync_products.json'), true);

        $chunkI = 0;
        $updateChunk = [];
        $batches = [];
        if (isset($fullBatch['update'])) { foreach ($fullBatch['update'] as $updateData) {
            $updateChunk[] = $updateData;
            $chunkI++;

            if ($chunkI == $chunkSize) {
                $batches[] = ['update'=>$updateChunk];
                $updateChunk = [];
                $chunkI = 0;
            }
        }}

        if ($updateChunk) {
            $batches[] = ['update'=>$updateChunk];
        }

        $chunkI = 0;
        $createChunk = [];
        if (isset($fullBatch['create'])) { foreach ($fullBatch['create'] as $createData) {
            $createData['status'] = 'draft';
            $createData['purchasable'] = false;
            $createData['lang'] = 'fr';
            $createChunk[] = $createData;
            $chunkI++;

            if ($chunkI == $chunkSize) {
                $batches[] = ['create'=>$createChunk];
                $createChunk = [];
                $chunkI = 0;
            }
        }}

        if ($createChunk) {
            $batches[] = ['create'=>$createChunk];
        }

        $type = '';
        $amount = 0;
        if (isset($batches[$page])) {
            if ( isset($batches[$page]['update']) ) {
                $wc->post('products/batch', $batches[$page]);
                $type = 'update';
                $amount = count($batches[$page]['update']);
            } else if ( isset($batches[$page]['create']) ) { 
                $wc->post('products/batch', $batches[$page]);
                $type = 'create';
                $amount = count($batches[$page]['create']);            
            } 
        } else {
            $type = 'DONE';
            $amount = 0;
        }

        $html = '<html><body>
        Type : '.$type.' | Count : '.$amount.'
        <script>
    setTimeout(function () {
        '.($type != 'DONE' ? 'window.location.href = "/bench?page='.($page+1).'";' : '').'
    }, 2000);

</script></body><html>';
        $response->getBody()->write($html);
        
        return $response
            ->withHeader('Content-Type', 'text/html')
            ->withStatus(200);
    });

    $app->get('/ordtest', function (Request $request, Response $response) {
        $response->getBody()->write(file_get_contents('./5_orders.json'));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write(json_encode(["Acomba/WooCommerce Sync API","WP Sync API"]));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(200);
    });
};
