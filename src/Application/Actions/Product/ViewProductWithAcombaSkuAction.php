<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use Psr\Http\Message\ResponseInterface as Response;

class ViewProductWithAcombaSkuAction extends ProductAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $productSKU = $this->resolveArg('sku');

        $this->logger->info("Product with Acomba SKU `${productSKU}` was viewed.");
        
        $product = $this->productRepository->getProductByAcombaSKU($productSKU);
        if ($product) {
            return $this->respondWithData($product);
        } else {
            return $this->respondWithData($product);
        }
    }
}
