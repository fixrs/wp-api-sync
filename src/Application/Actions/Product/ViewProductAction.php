<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use Psr\Http\Message\ResponseInterface as Response;

class ViewProductAction extends ProductAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $productId = (int) $this->resolveArg('id');
        $this->logger->info("Product with WP id `${productId}` was viewed.");
        
        $product = $this->productRepository->getProductById($productId);
        return $this->respondWithData($product);
    }
}
