<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use App\Application\Actions\Action;
use Psr\Http\Message\ResponseInterface as Response;

class UpdateProductAction extends Action
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $this->logger->info("Product was updated");

        return $this->respondWithData(["Update product action"]);
    }
}
