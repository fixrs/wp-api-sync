<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use App\Application\Actions\Action;
use Psr\Http\Message\ResponseInterface as Response;

class AddProductAction extends Action
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $this->logger->info("New product was added");

        return $this->respondWithData(["Add product action"]);
    }
}
