<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use Psr\Http\Message\ResponseInterface as Response;

class ViewAllSKUsWithIdsAction extends ProductAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $this->logger->info("Listed all products SKUs with IDs");
        
        $products = $this->productRepository->getAllProductSKUsWithIds();
        
        return $this->respondWithData($products);
    }
}
