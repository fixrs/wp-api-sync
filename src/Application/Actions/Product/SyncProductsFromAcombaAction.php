<?php
declare(strict_types=1);

namespace App\Application\Actions\Product;

use App\Application\Actions\Action;
use Psr\Http\Message\ResponseInterface as Response;

class SyncProductsFromAcombaAction extends Action
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $this->logger->info("All products were synced");

        return $this->respondWithData(["Sync products action"]);
    }
}
