<?php
declare(strict_types=1);

namespace App\Application\Actions\Client;

use Psr\Http\Message\ResponseInterface as Response;

class ViewClientActionWithPhone extends ClientAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $clientPhone = (string) $this->resolveArg('phone');

        $this->logger->info("Client with phone # `${clientPhone}` was viewed.");
        
        $client = $this->clientRepository->getClientByPhone($clientPhone);
        if ($client) {
            return $this->respondWithData($client);
        } else {
            return $this->respondWithData($client);
        }
    }
}
