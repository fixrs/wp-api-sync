<?php
declare(strict_types=1);

namespace App\Application\Actions\Group;

use Psr\Http\Message\ResponseInterface as Response;
use PDO;

class ListAllGroupsAction extends GroupAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $stmtUserGroupsWithPriceIndex = $this->db->prepare(
            'SELECT t.term_id AS id, t.name AS title, tm.meta_value AS price_index
            FROM cc_termmeta AS tm
            LEFT JOIN cc_terms AS t ON t.term_id = tm.term_id
            WHERE tm.meta_key LIKE "_user_group_discount"
            ORDER BY t.name'
        );
        if( !$stmtUserGroupsWithPriceIndex->execute()) {
            $this->logger->info("Failed listing all groups.");
            die('Execute on User Groups failed');
        }

        $this->logger->info("Groups were listed.");

        $userGroupsWithPriceIndex = $stmtUserGroupsWithPriceIndex->fetchAll(PDO::FETCH_ASSOC);

        return $this->respondWithData($userGroupsWithPriceIndex);
    }
}
