<?php
declare(strict_types=1);

namespace App\Application\Actions\Group;

use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;
use PDO;

abstract class GroupAction extends Action
{
    /**
     * @var PDO The database connection
     */
    protected $db;

    /**
     * @param LoggerInterface $logger
     * @param PDO $dbConnection
     */
    public function __construct(
        LoggerInterface $logger,
        PDO $dbConnection
    ) {
        parent::__construct($logger);
        $this->db = $dbConnection;
    }
}
