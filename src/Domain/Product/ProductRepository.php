<?php

namespace App\Domain\Product;

use App\Domain\Product\ProductNotFoundException;
use PDO;
use Automattic\WooCommerce\Client as WooCommerceClient;

/**
 * Repository.
 */
final class ProductRepository
{
    /**
     * @var PDO The database connection
     */
    private $db_connection;

    /**
     * @var WooCommerceClient The WooCommerce API connection
     */
    private $wc_api_connection;

    /**
     * @var Array Configuration to convert data in the XX_posts table (not all of the data from XX_posts is wanted for Acomba)
     */
    private $convertPostData = array(
        'ID'=>'id',
        'post_title'=>'title',
        'post_status'=>'status'
    );

    /**
     * @var Array Configuration to convert data in the XX_postmeta table for Acomba (not all of the data from XX_posts is wanted for Acomba)
     */
    private $convertPostMetaData= array(
        '_sku'=>'sku',
        '_stock'=>'stock'
    );

    /**
     * Constructor.
     *
     * @param PDO $p_db_connection The database connection
     */
    public function __construct(PDO $p_db_connection, WooCommerceClient $p_wc_api_connection)
    {
        $this->db_connection = $p_db_connection;
        $this->wc_api_connection = $p_wc_api_connection;
    }

    public function getProductById(int $p_id): ?object
    {
        return $this->wc_api_connection->get('products/'.$p_id);
    }

    public function getProductByAcombaSKU(string $p_SKU): ?object
    {
        $productSelectStmt = $this->db_connection->prepare(
            'SELECT p_meta.post_id AS id
            FROM cc_postmeta AS p_meta 
            LEFT JOIN cc_icl_translations AS t ON t.element_id = p_meta.post_id AND t.element_type IN ("post_product", "post_product_variation")
            WHERE 
            p_meta.meta_value = :SKU AND t.language_code = "fr" LIMIT 1'
        );
        $productSelectStmt->bindValue(':SKU', $p_SKU);

        if (!$productSelectStmt->execute()) {
            throw new ProductNotFoundException();
        }

        $product = $productSelectStmt->fetch(PDO::FETCH_ASSOC);
        if (!$product) {
            throw new ProductNotFoundException();
        } 

        return $this->getProductById($product['id']);
    }

    public function getAllProductSKUsWithIds(): ?array
    {
        $productSelectStmt = $this->db_connection->prepare(
            'SELECT 
                p.id AS id, 
                p_sku.meta_value AS sku,
                p.post_title AS title,
                p.post_type AS wp_type,
                p_stock.meta_value AS stock,
                t.language_code AS lang,
                p.post_status AS `status`,
                p.post_parent AS `parent`
            FROM cc_postmeta AS p_sku
            LEFT JOIN cc_posts AS p ON p.ID = p_sku.post_id
            LEFT JOIN cc_postmeta AS p_stock ON p_stock.post_id = p_sku.post_id AND p_stock.meta_key = "_stock"
            LEFT JOIN cc_icl_translations AS t ON t.element_id = p_sku.post_id AND t.element_type IN ("post_product", "post_product_variation")
            WHERE p_sku.meta_key = "_sku" AND LENGTH(p_sku.meta_value) > 0
            GROUP BY p_sku.post_id'
        );

        if (!$productSelectStmt->execute()) {
            throw new ProductNotFoundException();
        }

        $productList = [];
        $productVariations = [];
        while ($row = $productSelectStmt->fetch(PDO::FETCH_ASSOC)) {
            if ($row['wp_type'] == 'product_variation' && $row['parent']) {
                $productVariations[$row['parent']][] = [
                    'id'=>$row['id'], 
                    'sku'=>$row['sku']
                ];
            } 
            
            $productList[] = $row;
        }

        foreach ($productList as &$product) {
            if (array_key_exists($product['id'], $productVariations)) {
                $product['variations'] = $productVariations[$product['id']];
            }
        }

        if (!$productList) {
            throw new ProductNotFoundException();
        } 

        return $productList;
    }

    public function getAllProducts(): ?array 
    {
        return $this->wc_api_connection->get('products?per_page=10');
    }

}
