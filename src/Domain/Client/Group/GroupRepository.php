<?php

namespace App\Domain\Client;

use App\Domain\Client\ClientNotFoundException;
use PDO;

/**
 * Repository.
 */
final class ClientRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $p_connection)
    {
        $this->connection = $p_connection;
    }

    public function getClientById(int $p_id): ?array
    {
        $clientSelectStmt = $this->connection->prepare(
            'SELECT 
                u.ID, 
                ufn.meta_value AS first_name,
                uln.meta_value AS last_name,
                up.meta_value AS phone
            FROM cc_users AS u
            LEFT JOIN cc_usermeta AS ufn ON ufn.user_id = u.ID AND ufn.meta_key = "first_name"
            LEFT JOIN cc_usermeta AS uln ON uln.user_id = u.ID AND uln.meta_key = "last_name"
            LEFT JOIN cc_usermeta AS up ON up.user_id = u.ID AND up.meta_key = "billing_phone"
            WHERE u.ID = :id'
        );
        $clientSelectStmt->bindValue(':id', $p_id);

        if (!$clientSelectStmt->execute()) {
            throw new ClientNotFoundException();
        }

        $client = $clientSelectStmt->fetch(PDO::FETCH_ASSOC);
        if (!$client) {
            throw new ClientNotFoundException();
        }

        return $client;
    }

    public function getClientByPhone(string $p_phone): ?array 
    {
        $clientSelectStmt = $this->connection->prepare(
            'SELECT id, first_name, last_name, 
                CONCAT(SUBSTR(phone, 1, 3), "-", SUBSTR(phone, 4, 3), "-", SUBSTR(phone, 7, 4)) AS phone
            FROM (
                SELECT 
                    u.ID AS id, 
                    ufn.meta_value AS first_name,
                    uln.meta_value AS last_name,
                    REGEXP_REPLACE(REGEXP_REPLACE(up.meta_value, "[^0-9]", ""), "^1", "")
                        AS phone
                FROM cc_users AS u
                LEFT JOIN cc_usermeta AS ufn ON ufn.user_id = u.ID AND ufn.meta_key = "first_name"
                LEFT JOIN cc_usermeta AS uln ON uln.user_id = u.ID AND uln.meta_key = "last_name"
                LEFT JOIN cc_usermeta AS up ON up.user_id = u.ID AND up.meta_key = "billing_phone"
                WHERE LENGTH(up.meta_value) > 9
            ) AS t
            WHERE phone = REGEXP_REPLACE(REPLACE(:phone, "-", ""), "^1", "")'
        );
        $clientSelectStmt->bindValue(':phone', $p_phone);

        if (!$clientSelectStmt->execute()) {
            throw new ClientNotFoundException();
        }

        $client = $clientSelectStmt->fetch(PDO::FETCH_ASSOC);
        if (!$client) {
            throw new ClientNotFoundException();
        }

        return $client;
    }

}
